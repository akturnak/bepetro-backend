from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from datetime import datetime
from random import randint
from django.db.models import Count
import redis



@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class AnekdotsManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        count_index = count
        published = True
        while published:
            random_index = randint(0, count - 1)
            anekdot = self.all()[random_index]
            if not anekdot.published:
                return anekdot
            count_index -= 1
            if count_index<=0:
                published = False
        return None

class ParsedPhotoManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        count_index = count
        while True:
            random_index = randint(0, count - 1)
            photo = self.all()[random_index]
            if not photo.published:
                return photo
            count_index -= 1
            if count_index<=0:
                break
        return None

class Rubric(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class Anekdot(models.Model):
    objects = AnekdotsManager()
    text = models.TextField()
    rubric = models.ForeignKey(Rubric)
    published = models.BooleanField(default=False)

class ParsedPhoto(models.Model): # model for photos parsed from VK
    SOURCE_LEPRUM = -30022666
    SOURCE_HUMOR = -40895450
    SOURCE_SHREDCAT = -31076033
    SOURCE_VINNI_PUH = -64644733

    objects = ParsedPhotoManager()

    source_id = models.IntegerField() # public id
    source_post_id = models.IntegerField() # post id from public
    source_photo_id = models.IntegerField(default=0) # photo id (for generation source url)
    src_big = models.URLField()
    src_xbig = models.URLField()
    src_xxbig = models.URLField()
    dhash = models.CharField(max_length=16,default='',unique=True)
    published = models.BooleanField(default=False)


class Account(models.Model):
    secret = models.CharField(max_length=50)
    user = models.OneToOneField(User)

class Post(models.Model):
    SOURCE_ANEKDOT = 1
    SOURCE_LEPRUM_PHOTO = 2

    post_type = models.CharField(max_length=10, default='post') # todo: change to fixed values (tuples)
    pub_date = models.DateTimeField(default=datetime.now) # todo: check Django warning about timezone
    text = models.TextField(default='BePetro') # todo: change default value to apropriate
    anekdot = models.ForeignKey(Anekdot, blank=True, null=True)
    photo = models.ForeignKey(ParsedPhoto, blank=True, null=True)
    source_id = models.IntegerField(default=SOURCE_ANEKDOT) # available sources: 1 - Anekdots base, 2 - Photo parsed from VK
    likes = models.IntegerField(default=0)

class Favorite(models.Model):
    account = models.ForeignKey(Account)
    post = models.ForeignKey(Post)
    added = models.DateTimeField(default=datetime.now)

    class Meta:
        unique_together = ('account', 'post')

@receiver(post_save, sender=Post)
def set_last_post_date_to_cache(sender, instance=None, created=False, **kwargs):
    if created:
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        last_post_date = int(instance.pub_date.timestamp())
        r.set('last_post_date', last_post_date) # set last post date - no matter what type of post
        # set last_post_date by post_type
        if instance.post_type=='post':
            r.set('last_post_date_post', last_post_date)
        elif instance.post_type=='long_post':
            r.set('last_post_date_long_post', last_post_date)
        elif instance.post_type=='photo':
            r.set('last_post_date_photo', last_post_date)