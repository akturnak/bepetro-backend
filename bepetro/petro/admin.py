from django.contrib import admin

from .models import Anekdot, Rubric, ParsedPhoto, Account, Post, Favorite

admin.site.register(Rubric)
admin.site.register(Account)
admin.site.register(Favorite)


class AnekdotAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ("text", "rubric", "published")
    list_filter = ("published",)
    list_editable = ("rubric",)
    search_fields = ("text",)


class PostAdmin(admin.ModelAdmin):
    list_select_related = True
    date_hierarchy = 'pub_date'


class ParsedPhotoAdmin(admin.ModelAdmin):
    list_select_related = True


admin.site.register(Anekdot, AnekdotAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(ParsedPhoto, ParsedPhotoAdmin)
