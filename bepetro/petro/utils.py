import io
import requests
import datetime
import hashlib
import random

import imagehash
from PIL import Image

import petro.utils
from petro.models import Anekdot, Post, ParsedPhoto


def hamming_distance(s1, s2):
    """Calculate the Hamming distance between two bit strings"""
    assert len(s1) == len(s2)
    return sum(c1 != c2 for c1, c2 in zip(s1, s2))


def dhash_for_photo(url):
    """ Calculate dHash for photo by url """
    try:
        response = requests.get(url)
    except requests.exceptions.ConnectionError:
        print('Connection Error')
        return None
    img = Image.open(io.BytesIO(response.content))
    return str(imagehash.dhash(img))


def is_photo_unique(url):  # todo: need optimaze in future
    """ search photo in database (check dhash with hamming distance) """
    hash = dhash_for_photo(url)
    for photo in ParsedPhoto.objects.all():
        if hamming_distance(photo.dhash, hash) < 4:
            return False
    return True


def random_number(length):
    nums = '0123456789'
    return ''.join(random.choice(nums) for i in range(length))


def random_anekdot():
    return Anekdot.objects.random()


def random_photo():
    return ParsedPhoto.objects.random()


def anekdot_to_post(anekdot):
    if len(anekdot.text) < 1000:
        post_type = 'post'
    else:
        post_type = 'long_post'
    post = Post.objects.create(post_type=post_type, pub_date=datetime.datetime.utcnow(), text=anekdot.text,
                               anekdot=anekdot, source_id=Post.SOURCE_ANEKDOT)
    anekdot.published = True
    anekdot.save()
    return post


def generate_dhash_for_all_photos():
    photos = ParsedPhoto.objects.all()
    for photo in photos:
        hash = dhash_for_photo(photo.src_big)
        if hash:
            photo.dhash = hash
            photo.save()
