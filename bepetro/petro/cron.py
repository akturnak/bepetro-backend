import datetime
from random import randint

from django.db.models import F

import vk

import petro.utils
from petro.utils import is_photo_unique, dhash_for_photo
from petro.models import Rubric, Anekdot, Post, ParsedPhoto

def generate_post():
    anekdot = petro.utils.random_anekdot()
    post = petro.utils.anekdot_to_post(anekdot)


def generate_photo_post():
    photo = petro.utils.random_photo()
    photo.published = True
    photo.save()
    Post.objects.create(post_type='photo', pub_date=datetime.datetime.utcnow().replace(microsecond=0), text='',
                        photo=photo, source_id=photo.source_id)


def add_random_likes():
    time_24_hours_ago = datetime.datetime.now() - datetime.timedelta(days=1)
    posts = Post.objects.filter(pub_date__gte=time_24_hours_ago)
    for post in posts:
        if (randint(0, 3) == 1):
            Post.objects.filter(pk=post.id).update(likes=F("likes") + randint(0, 2))


def parse_photos():
    parse_photo_from_vk(ParsedPhoto.SOURCE_LEPRUM)
    parse_photo_from_vk(ParsedPhoto.SOURCE_HUMOR)
    parse_photo_from_vk(ParsedPhoto.SOURCE_SHREDCAT)
    parse_photo_from_vk(ParsedPhoto.SOURCE_VINNI_PUH)


def parse_photo_from_vk(public_id, posts_count_for_parse=20):
    session = vk.Session()
    api = vk.API(session)
    wall = api.wall.get(owner_id=public_id, count=posts_count_for_parse)
    wall.pop(0)  # remove first element (count of posts on this public)

    items = []  # temporary list (can have already posted photos)
    for item in wall:
        if item['post_type'] == 'post' and not item['text']:
            if len(item['attachments']) == 1:
                items.append(item)

    new_items = []
    for item in items:
        if item['attachments'][0]['photo']['src_big'] and is_photo_unique(
                item['attachments'][0]['photo']['src_big']):  # check photo for uniqness
            new_items.append(item)

    if new_items:
        for item in new_items:
            source_post_id = item['id']
            try:
                source_photo_id = item['attachments'][0]['photo']['pid']
            except KeyError:
                source_photo_id = None
            try:
                src_big = item['attachments'][0]['photo']['src_big']
            except KeyError:
                src_big = ''
            try:
                src_xbig = item['attachments'][0]['photo']['src_xbig']
            except KeyError:
                src_xbig = ''
            try:
                src_xxbig = item['attachments'][0]['photo']['src_xxbig']
            except KeyError:
                src_xxbig = ''
            # even if bigger photo urls is available - whatever cancel creation
            if src_big and src_big != '' and source_photo_id:
                hash = dhash_for_photo(src_big)
                ParsedPhoto.objects.create(source_id=public_id, source_post_id=source_post_id,
                                           source_photo_id=source_photo_id, src_big=src_big, src_xbig=src_xbig,
                                           src_xxbig=src_xxbig, dhash=hash)
