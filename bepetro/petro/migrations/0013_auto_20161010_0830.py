# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-10 08:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('petro', '0012_parsedphoto_source_photo_id'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='favorite',
            unique_together=set([('account', 'post')]),
        ),
    ]
