import datetime

from django.test import TestCase

from petro.models import Rubric, Anekdot, Post
from petro.cron import generate_post
import petro.utils


class RubricAnekdotTestCase(TestCase):
    def setUp(self):
        rubric_blonde = Rubric.objects.create(name='Блондинки')
        rubric_chukcha = Rubric.objects.create(name='Чукча')
        Anekdot.objects.create(text='текст анекдота 1', rubric=rubric_blonde)
        Anekdot.objects.create(text='текст анекдота 55', rubric=rubric_chukcha)
        Anekdot.objects.create(text='текст анекдота 768', rubric=rubric_chukcha)
        Anekdot.objects.create(text='текст анекдота 454', rubric=rubric_chukcha)

    def test_rubric(self):
        """Check what rubrics are exist"""
        self.assertEqual(len(Anekdot.objects.all()), 4)
        self.assertEqual(len(Rubric.objects.all()), 2)
        rubric_blonde = Rubric.objects.get(name='Блондинки')
        rubric_chukcha = Rubric.objects.get(name='Чукча')
        anekdot_blonde = Anekdot.objects.filter(rubric=rubric_blonde)
        anekdot_chukcha = Anekdot.objects.filter(rubric=rubric_chukcha)
        self.assertEqual(len(anekdot_blonde), 1)
        self.assertEqual(len(anekdot_chukcha), 3)
        self.assertEqual(anekdot_blonde[0].text, 'текст анекдота 1')


class PostTestCase(TestCase):
    def setUp(self):
        pass

    def test_post(self):
        Post.objects.create(post_type='post', pub_date=datetime.datetime.now(),
                            text='Южный курорт. Богатый фруктовый развал посреди улицы. Сонная продавщица отгоняет мух.')
        Post.objects.create(post_type='post', pub_date=datetime.datetime.now(), text='текст анекдота 1')
        Post.objects.create(post_type='postt', pub_date=datetime.datetime.now(),
                            text='текст анекдота 2')  # for future error
        self.assertEqual(len(Post.objects.all()), 3)


class UtilsTestCase(TestCase):
    def setUp(self):
        rubric_blonde = Rubric.objects.create(name='Блондинки')
        rubric_chukcha = Rubric.objects.create(name='Чукча')
        Anekdot.objects.create(text='текст анекдота 1', rubric=rubric_blonde)
        Anekdot.objects.create(text='текст анекдота 55', rubric=rubric_chukcha)
        Anekdot.objects.create(text='текст анекдота 768', rubric=rubric_chukcha)
        Anekdot.objects.create(text='текст анекдота 454', rubric=rubric_chukcha)

    def test_random_number(self):
        self.assertEqual(len(petro.utils.random_number(9)), 9)

    def test_random_anekdot(self):
        anekdot = petro.utils.random_anekdot()
        self.assertIsNotNone(anekdot)

    def test_random_generate_post(self):
        count = len(Anekdot.objects.filter(published=False))  # count of not published anekdots
        anekdot = petro.utils.random_anekdot()
        self.assertEqual(anekdot.published, False)
        post = petro.utils.anekdot_to_post(anekdot)
        self.assertIsNotNone(post)
        self.assertEqual(anekdot.published, True)
        self.assertEqual(count - 1, len(Anekdot.objects.filter(published=False)))

    def test_cron_generate_post(self):
        count_anekdots = len(Anekdot.objects.filter(published=False))  # count of not published anekdots
        count_posts = len(Post.objects.all())  # count of publications
        generate_post()
        self.assertEqual(count_anekdots - 1, len(Anekdot.objects.filter(published=False)))
        self.assertEqual(count_posts + 1, len(Post.objects.all()))
