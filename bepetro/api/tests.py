import datetime

from django.contrib.auth.models import User
from django.db.models.functions import Length

from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from rest_framework.authtoken.models import Token

from petro.utils import anekdot_to_post
from petro.models import Account, Post, Anekdot, Rubric, Favorite
from petro import cron


class AuthTest(APITestCase):
    def setUp(self):
        self.client_id = '567833'
        self.secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'

    def test_get_token_good_parameters(self):
        response = self.client.get('/api/v1/auth/access_token', {'client_id': self.client_id, 'secret': self.secret})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        account = Account.objects.get(secret=self.secret)
        self.assertEqual(account.secret, self.secret)
        self.assertEqual(response.data['access_token'], Token.objects.get(user=account.user).key)

    def test_get_token_bad_parameters(self):
        response = self.client.get('/api/v1/auth/access_token')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get('/api/v1/auth/access_token', {'client_id': self.client_id})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get('/api/v1/auth/access_token', {'secret': self.secret})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get('/api/v1/auth/access_token', {'client_id': self.client_id, 'secret': ''})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get('/api/v1/auth/access_token', {'client_id': '', 'secret': self.secret})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_feed_without_auth(self):
        response = self.client.get('/api/v1/feed')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class FeedTest(APITestCase):
    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        ''' setup test data '''

        # add post
        self.posts = []
        self.long_posts = []
        post_hour = datetime.datetime.now().hour - 1  # because if we set it to current - it can generate fucking error
        # when end_time generated on server less then last post.pub_date
        if post_hour < 0:
            post_hour = 0

        # generate post
        for i in range(0, 58):
            post_text = 'текст анекдота ' + str(i)
            self.posts.append(Post.objects.create(post_type='post',
                                                  pub_date=datetime.datetime.utcnow().replace(hour=post_hour, minute=i,
                                                                                              microsecond=0),
                                                  text=post_text))

        self.last_post = self.posts[len(self.posts) - 1]
        self.last_post.likes = 2
        self.last_post.save()
        for i in range(30):
            self.client.post('/api/v1/posts/{}/likes/add'.format(self.posts[-i].id))

    def test_get_feed_default_parameters(self):
        response = self.client.get('/api/v1/feed')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        self.assertContains(response, 'next_from')
        items = response.data['items']
        self.assertIsNotNone(items)
        self.assertEqual(len(items), 50)
        item = items[0]
        self.assertIsNotNone(item)
        self.assertIsNotNone(item['post_type'])
        self.assertIsNotNone(item['source_id'])
        self.assertIsNotNone(item['source_url'])
        self.assertEqual(item['source_url'], '')
        self.assertIsNotNone(item['date'])
        self.assertIsNotNone(item['post_id'])
        self.assertIsNotNone(item['favorite'])
        self.assertIsNotNone(item['likes'])
        self.assertIsNotNone(item['text'])
        self.assertIsNotNone(item['attachments'])
        list_for_post = [p for p in self.posts if p.id == item['post_id']]
        self.assertIsNotNone(list_for_post[0])
        self.assertIsNotNone(item['date'])
        item_pub_date = datetime.datetime.fromtimestamp(item['date'])
        self.assertIsNotNone(item_pub_date)
        self.assertEqual(item_pub_date, list_for_post[0].pub_date)
        self.last_post.refresh_from_db()
        self.assertEqual(item['likes'], self.last_post.likes)
        self.assertEqual(bool(item['favorite']), True)  # in Setup we "like" this post
        item2 = items[1]
        self.assertEqual(bool(item2['favorite']), True)  # user did not "like" this post

    def test_get_feed_custom_parameters(self):  # count and start_from

        count1 = 10
        response = self.client.get('/api/v1/feed', {'count': count1})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data['items']
        self.assertEqual(len(items), count1)
        self.assertEqual(items[0]['post_id'], self.posts[-1].id)  # last published must be first in returned list
        start_from = response.data['next_from']
        next_timestamp = int(self.posts[-count1].pub_date.timestamp()) - 1
        self.assertEqual(start_from, next_timestamp)

        #
        count2 = 30
        response = self.client.get('/api/v1/feed', {'count': count2, 'start_from': start_from})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data['items']
        self.assertEqual(len(items), count2)
        start_from = response.data['next_from']
        next_timestamp = int(self.posts[-(count1 + count2)].pub_date.timestamp()) - 1
        self.assertEqual(start_from, next_timestamp)

        #
        count3 = 20
        response = self.client.get('/api/v1/feed', {'count': count3, 'start_from': start_from})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data['items']
        self.assertEqual(len(items), (len(self.posts) - count1 - count2))
        start_from = response.data['next_from']
        self.assertEqual(start_from, None)

    def test_get_feed_time_interval(self):  # start_time and end_time

        # without end_time
        count = 10
        start_post = 20
        start_time = int(self.posts[start_post].pub_date.timestamp()) - 1
        response = self.client.get('/api/v1/feed', {'count': count, 'start_time': start_time})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data['items']
        self.assertEqual(len(items), 10)
        self.assertEqual(items[0]['post_id'], self.posts[len(self.posts) - 1].id)
        end_time = response.data['next_from']
        self.assertEqual(end_time, int(self.posts[-count].pub_date.timestamp()) - 1)

        # with end_time
        response = self.client.get('/api/v1/feed', {'count': count, 'start_time': start_time, 'end_time': end_time})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data['items']
        self.assertEqual(len(items), 10)
        self.assertEqual(items[0]['post_id'], self.posts[len(self.posts) - 11].id)

    def test_add_and_delete_likes(self):
        post = self.posts[1]
        post.likes = 3  # set test likes
        post.save()
        # test add like
        response = self.client.post('/api/v1/posts/{}/likes/add'.format(post.id))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        post.refresh_from_db()
        self.assertEqual(response.data['likes'], post.likes)
        response = self.client.post('/api/v1/posts/{}/likes/add'.format(post.id))  # block attempt to "like" twice
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        # test delete like
        response = self.client.delete('/api/v1/posts/{}/likes/delete'.format(post.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        post.refresh_from_db()
        self.assertEqual(response.data['likes'], post.likes)


class FavoriteTest(APITestCase):
    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        ''' setup test data '''

        # add photos todo: change to fake generated photos
        cron.parse_photos()
        posts = Post.objects.filter(post_type='photo')
        for post in posts:
            self.client.post('/api/v1/posts/{}/likes/add'.format(post.id))

        # add post
        self.posts = []
        self.long_posts = []
        post_hour = datetime.datetime.now().hour - 1  # because if we set it to current - it can generate fucking error
        # when end_time generated on server less then last post.pub_date
        if post_hour < 0:
            post_hour = 0

        # generate long_post
        for i in range(0, 58):
            post_text = 'текст длинного анекдота ' + str(i)
            self.long_posts.append(Post.objects.create(post_type='long_post',
                                                       pub_date=datetime.datetime.utcnow().replace(hour=post_hour,
                                                                                                   minute=i,
                                                                                                   microsecond=0),
                                                       text=post_text))

        # generate post
        for i in range(0, 58):
            post_text = 'текст анекдота ' + str(i)
            self.posts.append(Post.objects.create(post_type='post',
                                                  pub_date=datetime.datetime.utcnow().replace(hour=post_hour + 1,
                                                                                              minute=i, microsecond=0),
                                                  text=post_text))

        self.last_post = self.posts[len(self.posts) - 1]
        self.last_post.likes = 2
        self.last_post.save()
        for i in range(30):
            self.client.post('/api/v1/posts/{}/likes/add'.format(self.posts[-i].id))
        for j in range(30):
            self.client.post('/api/v1/posts/{}/likes/add'.format(self.long_posts[-j].id))

        user = Token.objects.get(key=token).user
        account = Account.objects.get(user=user)
        self.favorites = Favorite.objects.filter(account=account)
        self.favorite_all_count = len(self.favorites)
        self.favorite_post_count = len(self.favorites.filter(post__post_type='post'))
        self.favorite_photo_count = len(self.favorites.filter(post__post_type='photo'))
        self.favorite_long_post_count = len(self.favorites.filter(post__post_type='long_post'))

    def test_get_favorite_posts(self):
        # test for count parameter
        count = 10
        response = self.client.get('/api/v1/users/favorites', {'count': count})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        self.assertContains(response, 'count')
        self.assertEqual(response.data['count'], self.favorite_all_count)
        items = response.data['items']
        self.assertEqual(len(items), count)
        # self.assertEqual(items[0]['post_id'], self.favorites.order_by('-added')[self.favorites.count()-1].id)
        # test for offset parameter
        offset = 5
        count = 5
        response = self.client.get('/api/v1/users/favorites', {'count': count, 'offset': offset})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], self.favorite_all_count)
        self.assertContains(response, 'items')
        items = response.data['items']
        self.assertEqual(len(items), count)
        # self.assertEqual(items[0]['post_id'], self.posts[len(self.posts)-(1+offset)].id)
        # test for post filter
        count = 10
        filters = 'post'
        response = self.client.get('/api/v1/users/favorites', {'count': count, 'filters': filters})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], self.favorite_post_count)
        self.assertContains(response, 'items')
        items = response.data['items']
        for item in items:
            self.assertFalse(item['attachments'])
        # test for long_post filter
        count = 10
        filters = 'long_post'
        response = self.client.get('/api/v1/users/favorites', {'count': count, 'filters': filters})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], self.favorite_long_post_count)
        self.assertContains(response, 'items')
        items = response.data['items']
        self.assertEqual(len(items), count)
        for item in items:
            self.assertEqual(item['attachments'][0]['type'], 'long_post')
            self.assertTrue(item['attachments'][0]['items']['text'])
        # test for photo filter
        count = 10
        filters = 'photo'
        response = self.client.get('/api/v1/users/favorites', {'count': count, 'filters': filters})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], self.favorite_photo_count)
        self.assertContains(response, 'items')
        items = response.data['items']
        for item in items:
            self.assertEqual(item['attachments'][0]['type'], 'photo')
            self.assertTrue(item['attachments'][0]['items']['src_big'])


class RarsePhotoTest(APITestCase):
    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        cron.parse_photos()

    def test_get_photo_posts(self):
        response = self.client.get('/api/v1/feed')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        self.assertContains(response, 'next_from')
        items = response.data['items']
        self.assertIsNotNone(items)
        item = items[0]
        self.assertEqual(item['post_type'], 'post')
        self.assertFalse(item['source_url'])
        attachments = item['attachments']
        self.assertEqual(len(attachments), 1)
        self.assertEqual(attachments[0]['type'], 'photo')
        self.assertEqual(len(attachments[0]['items']), 3)
        self.assertEqual(type(attachments[0]['items']['src_big']), str)
        self.assertEqual(type(attachments[0]['items']['src_xbig']), str)
        self.assertEqual(type(attachments[0]['items']['src_xxbig']), str)


class LongPostTest(APITestCase):
    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        ''' setup test data '''
        rubric = Rubric.objects.create(name='Rubric 1')
        for i in range(10):
            text = ('x' * 1000) + str(i)
            Anekdot.objects.create(rubric=rubric, text=text)
        anekdots = Anekdot.objects.annotate(text_len=Length('text')).filter(text_len__gt=1000)
        for anekdot in anekdots:
            anekdot_to_post(anekdot)

    def test_get_long_post(self):
        response = self.client.get('/api/v1/feed')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        self.assertContains(response, 'next_from')
        items = response.data['items']
        self.assertIsNotNone(items)
        item = items[0]
        self.assertEqual(item['post_type'], 'post')
        self.assertEqual(item['source_url'], '')
        attachments = item['attachments']
        self.assertEqual(len(attachments), 1)
        self.assertEqual(attachments[0]['type'], 'long_post')
        self.assertEqual(len(attachments[0]['items']), 1)
        self.assertEqual(type(attachments[0]['items']['text']), str)


class FeedFilterTest(APITestCase):
    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        ''' setup test data '''
        rubric = Rubric.objects.create(name='Rubric 1')
        # create long_posts
        for i in range(5):
            text = ('x' * 1000) + str(i)
            Anekdot.objects.create(rubric=rubric, text=text)
        anekdots = Anekdot.objects.annotate(text_len=Length('text')).filter(text_len__gt=1000)
        for anekdot in anekdots:
            anekdot_to_post(anekdot)
        # create posts
        for i in range(5):
            text = ('y' * 500) + str(i)
            Anekdot.objects.create(rubric=rubric, text=text)
        anekdots = Anekdot.objects.annotate(text_len=Length('text')).filter(text_len__lt=1000)
        for anekdot in anekdots:
            anekdot_to_post(anekdot)

        # parse and create photo posts
        cron.parse_photos()

    def test_get_only_posts(self):
        response = self.client.get('/api/v1/feed', {'filters': 'post'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']
        for item in items:
            self.assertFalse(item['attachments'])

    def test_get_only_long_posts(self):
        response = self.client.get('/api/v1/feed', {'filters': 'long_post'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']
        for item in items:
            self.assertEqual(item['post_type'], 'post')
            self.assertTrue(item['attachments'])
            self.assertEqual(item['attachments'][0]['type'], 'long_post')
            self.assertTrue(item['attachments'][0]['items']['text'])

    def test_get_only_photos(self):
        response = self.client.get('/api/v1/feed', {'filters': 'photo'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']
        for item in items:
            self.assertEqual(item['post_type'], 'post')
            self.assertTrue(item['attachments'])
            self.assertEqual(item['attachments'][0]['type'], 'photo')
            self.assertTrue(item['attachments'][0]['items']['src_big'])

    def test_post_and_long_posts(self):
        filter_values = 'long_post, post'
        response = self.client.get('/api/v1/feed', {'filters': filter_values, 'count': 100})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']

    def test_posts_and_photo_and_long_posts(self):
        filter_values = 'long_post, post, photo'
        response = self.client.get('/api/v1/feed', {'filters': filter_values})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']
        for item in items:
            if item['attachments']:
                if item['attachments'][0]['type'] not in filter_values:
                    raise ValidationError('error')

    def test_photo_and_long_posts(self):
        filter_values = 'long_post, photo'
        response = self.client.get('/api/v1/feed', {'filters': filter_values})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']
        count = len(Post.objects.filter(post_type='photo') | Post.objects.filter(post_type='long_post'))
        self.assertEqual(len(items), count)
        for item in items:
            if not item['attachments']:
                raise ValidationError('error')

    def test_post_and_long_posts(self):
        filter_values = 'long_post, post'
        response = self.client.get('/api/v1/feed', {'filters': filter_values})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'items')
        items = response.data['items']
        count = len(Post.objects.filter(post_type='post') | Post.objects.filter(post_type='long_post'))
        self.assertEqual(len(items), count)


class DeveloperMagicLinkTest(APITestCase):
    '''
        Link for developer - out of turn post generation
    '''

    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        ''' Generate test Anekdots '''
        rubric = Rubric.objects.create(name='Rubric 1')
        # create long_posts
        for i in range(50):
            text = ('x' * 500) + str(i)
            Anekdot.objects.create(rubric=rubric, text=text)

    def test_generate_posts(self):
        count = 1
        response = self.client.post('/api/v1/dev/generate_post')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        posts = Post.objects.all()
        self.assertEqual(len(posts), count)
        response = self.client.post('/api/v1/dev/generate_post')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        posts = Post.objects.all()
        self.assertEqual(len(posts), count + 1)


class GetLastPostDateTest(APITestCase):
    '''
        API endpoint for getting last post date        
    '''

    def setUp(self):
        ''' client config '''
        client_id = '567833'
        secret = '7dc53df5-703e-49b3-8670-b1c468f47f1f'
        response = self.client.get('/api/v1/auth/access_token', {'client_id': client_id, 'secret': secret})
        token = response.data['access_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        ''' setup test data '''
        rubric = Rubric.objects.create(name='Rubric 1')
        # create long_posts
        for i in range(5):
            text = ('x' * 1000) + str(i)
            Anekdot.objects.create(rubric=rubric, text=text)
        anekdots = Anekdot.objects.annotate(text_len=Length('text')).filter(text_len__gt=1000)
        for anekdot in anekdots:
            anekdot_to_post(anekdot)
        # create posts
        for i in range(5):
            text = ('y' * 500) + str(i)
            Anekdot.objects.create(rubric=rubric, text=text)
        anekdots = Anekdot.objects.annotate(text_len=Length('text')).filter(text_len__lt=1000)
        for anekdot in anekdots:
            anekdot_to_post(anekdot)

        # parse and create photo posts
        cron.parse_photos()

    def test_get_no_filters(self):
        response = self.client.get('/api/v1/posts/last/date', {'filters': ''})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('/api/v1/posts/last/date')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'date')
        response_latest_date = response.data['date']
        true_latest_date = int(Post.objects.latest('pub_date').pub_date.timestamp())
        self.assertEqual(true_latest_date, response_latest_date)

    def test_get_filter_post(self):
        response = self.client.get('/api/v1/posts/last/date', {'filters': 'post'})
        response_latest_date = response.data['date']
        true_latest_date = int(Post.objects.filter(post_type='post').latest('pub_date').pub_date.timestamp())
        self.assertEqual(true_latest_date, response_latest_date)

    def test_get_filter_long_post(self):
        response = self.client.get('/api/v1/posts/last/date', {'filters': 'long_post'})
        response_latest_date = response.data['date']
        true_latest_date = int(Post.objects.filter(post_type='long_post').latest('pub_date').pub_date.timestamp())
        self.assertEqual(true_latest_date, response_latest_date)

    def test_get_filter_photo(self):
        response = self.client.get('/api/v1/posts/last/date', {'filters': 'photo'})
        response_latest_date = response.data['date']
        true_latest_date = int(Post.objects.filter(post_type='photo').latest('pub_date').pub_date.timestamp())
        self.assertEqual(true_latest_date, response_latest_date)

    def test_get_all_filters(self):
        response = self.client.get('/api/v1/posts/last/date', {'filters': 'photo, long_post,post'})
        response_latest_date = response.data['date']
        true_latest_date = int(Post.objects.latest('pub_date').pub_date.timestamp())
        self.assertEqual(true_latest_date, response_latest_date)
