import datetime
from datetime import timedelta

from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.functions import Length
from django.db.models import F
from django.db.utils import IntegrityError

from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import redis

from petro.utils import random_number, random_anekdot
from petro.cron import generate_post
from petro.models import Account, Post, Favorite
from api.serializers import PostSerializer


@api_view(['GET'])
def get_token(request):
    """
    Get access token.
    """
    client_id = request.query_params.get('client_id', None)
    secret = request.query_params.get('secret', None)
    if not secret or not client_id:
        return Response(status=400)
    user = User.objects.create_user(username=str(random_number(9)), password='password')
    account = Account(user=user, secret=secret).save()
    token = Token.objects.get(user=user)
    return Response({'access_token': token.key})


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_feed(request):
    """
    Get User's feed.
    """

    count = int(request.query_params.get('count', 50))  # if count not set - use default value (50)
    if count > 100:
        count = 100
    if count <= 0:
        count = 50

    # post_type filters (post, long_post, photo)
    filter_query = Post.objects.all()  # todo: its BAD
    filters = request.query_params.get('filters', None)
    if filters:  # todo: it's small bad!
        filters = [x.strip() for x in filters.split(',')]
        long_post_query = Post.objects.none()
        post_query = Post.objects.none()
        photo_query = Post.objects.none()
        exclude_filter = None
        for filter in filters:
            if 'long_post' in filter:
                exclude_filter = filter
                long_post_query = Post.objects.filter(post_type='long_post')
        if exclude_filter:
            filters.remove(exclude_filter)  # remove 'long_post' filter string because filter 'post' also can hit this
        if 'post' in filters:
            post_query = Post.objects.filter(post_type='post')
        if 'photo' in filters:
            photo_query = Post.objects.filter(post_type='photo')
        filter_query = post_query | long_post_query | photo_query
    # datetime filters
    start_from = request.query_params.get('start_from', None)
    start_time = request.query_params.get('start_time', None)

    if start_from:
        date_from = datetime.datetime.fromtimestamp(int(start_from))
        posts = filter_query.filter(pub_date__lte=date_from).order_by('-pub_date')[:count]
    elif start_time:
        start_time = datetime.datetime.fromtimestamp(int(start_time))
        end_time = request.query_params.get('end_time', None)
        if not end_time:
            end_time = datetime.datetime.utcnow().replace(microsecond=0)
        else:
            end_time = datetime.datetime.fromtimestamp(int(end_time))
        posts = filter_query.filter(pub_date__gte=start_time).filter(pub_date__lte=end_time).order_by('-pub_date')[
                :count]
    else:
        posts = filter_query.order_by('-pub_date')[:count]

    post_serializer = PostSerializer(posts, many=True, context={'request': request})

    try:
        next_from = int(posts[(count - 1)].pub_date.timestamp()) - 1  # int for trim from float
    except IndexError:
        next_from = None

    content = {'items': post_serializer.data, 'next_from': next_from}

    return Response(content)


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def post_add_like(request, post_id):
    """
    Add like to post.
    """
    try:
        post = Post.objects.get(pk=post_id)  # check is the Post with this id exist
    except Post.DoesNotExist:
        return Response(status=404)
    account = Account.objects.get(user=request.user)
    try:
        Favorite.objects.get(account=account,
                             post=post)  # check - may be user already set "like". If not - OK, else return error
    except Favorite.DoesNotExist:
        try:
            Favorite.objects.create(account=account, post=post)
        except IntegrityError:  # if unique pair (post and account) already exists - return error response
            return Response(status=409)
        Post.objects.filter(pk=post_id).update(likes=F("likes") + 1)
        post.refresh_from_db()
        return Response(status=201, data={'likes': post.likes})
    else:
        return Response(status=409)


@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def post_delete_like(request, post_id):
    """
    Delete like from post.
    """
    try:
        post = Post.objects.get(pk=post_id)  # check: is the Post with this id exist
    except Post.DoesNotExist:
        return Response(status=404)
    account = Account.objects.get(user=request.user)
    try:
        favorite = Favorite.objects.get(account=account,
                                        post=post)  # check: if user's like exist - delete, else return error
    except Favorite.DoesNotExist:
        return Response(status=404)
    else:
        favorite.delete()
        Post.objects.filter(pk=post_id).update(likes=F("likes") - 1)  # TODO: change this to execute only one query
        post.refresh_from_db()
        return Response(status=200, data={'likes': post.likes})


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def dev_generate_post(request):
    """
    Help for Developer - generate posts.
    """
    generate_post()
    return Response(status=201)


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_favorite(request):
    """
    Get User's favorite posts.
    """
    account = Account.objects.get(user=request.user)
    # post_type filters (post, long_post, photo)
    filter_query = Favorite.objects.filter(account=account)
    filters = request.query_params.get('filters', None)
    if filters:
        filters = [x.strip() for x in filters.split(',')]
        exclude_filter = None
        long_post_query = Favorite.objects.none()
        post_query = Favorite.objects.none()
        photo_query = Favorite.objects.none()
        for filter in filters:
            if 'long_post' in filter:
                exclude_filter = filter
                long_post_query = filter_query.filter(
                    post__post_type='long_post')  # Post.objects.filter(post_type='long_post')
        if exclude_filter:
            filters.remove(exclude_filter)  # remove 'long_post' filter string because filter 'post' also can hit this
        if 'post' in filters:
            post_query = filter_query.filter(post__post_type='post')
        if 'photo' in filters:
            photo_query = filter_query.filter(post__post_type='photo')
        filter_query = post_query | long_post_query | photo_query

    count = int(request.query_params.get('count', 50))
    if count > 100:
        count = 100
    offset = int(request.query_params.get('offset', 0))
    filter_query = filter_query.order_by('-added')
    favorite_count = len(filter_query)
    favorites = filter_query[offset:(count + offset)]
    posts = []
    for favorite in favorites:
        posts.append(favorite.post)
    serializer = PostSerializer(posts, many=True, context={'request': request})
    content = {'items': serializer.data, 'count': favorite_count}
    return Response(content)


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_last_post_date(request):
    """
    Get last post date.
    """

    # prepare Redis connection
    r = redis.StrictRedis(host='localhost', port=6379, db=0)

    # post_type filters (post, long_post, photo)    
    filters = request.query_params.get('filters', None)

    if not filters:  # if no filters - get last post pub_date
        if r.get('last_post_date'):
            last_date = int(r.get('last_post_date'))
        else:
            last_date = int(Post.objects.latest('pub_date').pub_date.timestamp())
            r.set('last_post_date', last_date)
    else:  # if filters not empty - check latest by post_type
        filters = [x.rstrip() for x in filters.split(',')]
        last_date = 0

        if 'post' in filters:

            if r.get('last_post_date_post'):
                post_date = int(r.get('last_post_date_post'))
            else:
                post_date = int(Post.objects.filter(post_type='post').latest('pub_date').pub_date.timestamp())
                r.set('last_post_date_post', post_date)
            last_date = post_date if last_date < post_date else last_date

        if 'long_post' in filters:
            if r.get('last_post_date_long_post'):
                post_date = int(r.get('last_post_date_long_post'))
            else:
                post_date = int(Post.objects.filter(post_type='long_post').latest('pub_date').pub_date.timestamp())
                r.set('last_post_date_long_post', post_date)
            last_date = post_date if last_date < post_date else last_date

        if 'photo' in filters:
            if r.get('last_post_date_photo'):
                post_date = int(r.get('last_post_date_photo'))
            else:
                post_date = int(Post.objects.filter(post_type='photo').latest('pub_date').pub_date.timestamp())
                r.set('last_post_date_photo', post_date)
            last_date = post_date if last_date < post_date else last_date

    content = {'date': last_date}

    return Response(content)


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_top(request):
    """
    Get top posts.
    """

    count = 100

    # interval (day, week, month)
    interval_parameter = request.query_params.get('interval', 'day')
    if interval_parameter == 'week':
        interval = timezone.now().date() - timedelta(days=7)
    elif interval_parameter == 'month':
        interval = timezone.now().date() - timedelta(days=30)  # TODO: month may be shorter then 30 days!
    elif interval_parameter == 'day':
        interval = timezone.now().date() - timedelta(days=1)
    else:
        return Response(status=400)

    likes = Favorite.objects.filter(added__gte=interval)
    posts = Post.objects.filter(favorite__in=likes).filter(pub_date__gte=interval).distinct().order_by('pub_date')[
            :count]

    post_serializer = PostSerializer(posts, many=True, context={'request': request})

    try:
        next_from = int(posts[(count - 1)].pub_date.timestamp()) - 1  # int for trim from float
    except IndexError:
        next_from = None

    content = {'items': post_serializer.data}

    return Response(content)
