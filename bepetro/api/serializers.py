from rest_framework import serializers

from petro.models import Post, Favorite, Account, ParsedPhoto


class PostSerializer(serializers.ModelSerializer):  # todo: make validator for date field
    post_id = serializers.IntegerField(source='id')
    date = serializers.SerializerMethodField('date_to_timestamp')
    favorite = serializers.SerializerMethodField()
    attachments = serializers.SerializerMethodField()
    text = serializers.SerializerMethodField()
    post_type = serializers.SerializerMethodField()
    source_url = serializers.SerializerMethodField()

    def get_source_url(self, post):
        return ''
        # temporary disabled source url functionality
        if post.post_type == 'photo':
            url = 'http://vk.com/leprum?z=photo{}_{}'.format(str(ParsedPhoto.SOURCE_LEPRUM),
                                                             str(post.photo.source_photo_id))
            return url
        else:
            return ''

    def get_post_type(self, post):
        return 'post'

    def get_text(self, post):  # for split long posts from short/ long posts contains text in attachments
        if post.post_type == 'post':
            return post.text
        else:
            return ''

    def get_attachments(self, post):
        if post.post_type == 'photo':
            result = []
            photos = {}
            photos['type'] = 'photo'
            photos['items'] = {}
            photos['items']['src_big'] = post.photo.src_big
            photos['items']['src_xbig'] = post.photo.src_xbig
            photos['items']['src_xxbig'] = post.photo.src_xxbig
            result.append(photos)
            return result
        elif post.post_type == 'long_post':
            result = []
            long_post = {}
            long_post['type'] = 'long_post'
            long_post['items'] = {}
            long_post['items']['text'] = post.text
            result.append(long_post)
            return result
        else:
            return []

    def get_favorite(self, post):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        account = Account.objects.get(user=user)  # todo: think, if we does not have existing account?
        try:
            Favorite.objects.get(account=account, post=post)
        except Favorite.DoesNotExist:
            return False
        else:
            return True

    def date_to_timestamp(self, post):
        return int(post.pub_date.timestamp())

    class Meta:
        model = Post
        fields = ('post_id', 'source_id', 'source_url', 'post_type', 'date', 'favorite', 'likes', 'text', 'attachments')
