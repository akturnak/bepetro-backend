import sqlite3
import psycopg2

sqlite_file = '/home/nomad/projects/bepetro/db/anekdot'
conn_string = "host='localhost' dbname='bepetro' user='bepetro' password='1234'"

con_sqlite = sqlite3.connect(sqlite_file)
con_post = psycopg2.connect(conn_string)


cursor_sqlite_word = con_sqlite.cursor()
cursor_sqlite_phrase = con_sqlite.cursor()

cursor_post_rubric = con_post.cursor()
cursor_post_anekdot = con_post.cursor()

cursor_post_rubric.execute("DELETE FROM petro_rubric")
cursor_post_anekdot.execute("DELETE FROM petro_anekdot")
con_post.commit()

table_word = 'word'

cursor_sqlite_word = cursor_sqlite_word.execute('SELECT * FROM {tn}'.format(tn=table_word))
for row in cursor_sqlite_word:
  old_id = row[0]
  name = row[1]
  cursor_post_rubric.execute("INSERT INTO petro_rubric (name) VALUES (%s) RETURNING id", (name,))
  con_post.commit()
  id = cursor_post_rubric.fetchone()[0]
  cursor_sqlite_phrase = cursor_sqlite_phrase.execute('SELECT * FROM {tn} WHERE word={id}'.format(tn='phrase', id=old_id))
  for row2 in cursor_sqlite_phrase:
    #st = "INSERT INTO petro_anekdot (text, rubric_id) VALUES (%s, %s)", (row2[1], id)
    #print(st)
    cursor_post_anekdot.execute("INSERT INTO petro_anekdot (text, rubric_id) VALUES (%s, %s)", (row2[1][10:], id))
    con_post.commit()
