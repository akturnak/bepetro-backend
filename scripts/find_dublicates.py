import io
import requests
from PIL import Image
import imagehash

def hamming_distance(s1, s2):
	"""Calculate the Hamming distance between two bit strings"""
	assert len(s1) == len(s2)
	return sum(c1 != c2 for c1, c2 in zip(s1, s2))


url1 = 'http://cs636620.vk.me/v636620324/1f601/zW48I9EDy-w.jpg'
url2 = 'http://cs636620.vk.me/v636620324/1f600/USQa7CXVGmY.jpg'
url3 = 'http://cs636620.vk.me/v636620324/1f5ff/eXYm_YK-eFk.jpg'

response = requests.get(url1)
img1 = Image.open(io.BytesIO(response.content))

response = requests.get(url2)
img2 = Image.open(io.BytesIO(response.content))

response = requests.get(url3)
img3 = Image.open(io.BytesIO(response.content))

h1 = str(imagehash.dhash(img1))
h2 = str(imagehash.dhash(img2))
h3 = str(imagehash.dhash(img3))

if hamming_distance(h1, h2) < 4:
	print('SAME PICTURES')
else:
	print('DIFFERENT PICTURES')

'''
    SELECT pk, hash, file_path FROM image_hashes
        WHERE hash = '4c8e3366c275650f';

    SELECT pk, hash, BIT_COUNT(
        CONV(hash, 16, 10) ^ CONV('4c8e3366c275650f', 16, 10)
    ) as hamming_distance
        FROM image_hashes
        HAVING hamming_distance < 4
        ORDER BY hamming_distance ASC;
'''
 
from django.db.models import Count
ParsedPhoto.objects.values('dhash').annotate(Count('id')).order_by().filter(id__count__gt=1)

for dhash in ParsedPhoto.objects.values_list('dhash', flat=True).distinct():
    ParsedPhoto.objects.filter(pk__in=ParsedPhoto.objects.filter(dhash=photo.dhash).values_list('id', flat=True)[1:]).delete()


import timeit
from petro.cron import parse_photos
timeit.timeit(parse_photos)

https://pp.vk.me/c637121/v637121233/1a87e/I9qG7HATQNY.jpg